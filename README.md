# jms2rdb

[Docker] image to run [JMS2RDB](http://cs-studio.sourceforge.net/docbook/ch35.html),
a tool that listens to JMS messages from the log, alarm system or other CSS applications
and sends them to a database.

## Environment Variables

This image uses environment variables to create the plugin_customization.ini file passed the JMS2RDB.

Mandatory variables:

- `JMS2RDB_JMS_URL`: JMS URL where tool listens for messages to log (example: failover:(tcp://localhost:61616))
- `JMS2RDB_RDB_URL`: database connection URL (example: jdbc:mysql://localhost/log)
- `JMS2RDB_RDB_USER`: database user
- `JMS2RDB_RDB_PASSWORD`: database password

Optional variables:

- `JMS2RDB_JMS_TOPIC`: list of topics to log, separated by ',' [default: LOG]
- `JMS2RDB_JMS_FILTERS`: filters for suppressed JMS messages
  (format: `<Type>;<Property>=<Pattern>, <Type>;<Property>=<Pattern>`)
  [default: LOG;TEXT=JCACommandThread queue reached]

## How to use this image

```
$ docker run \
    -e JMS2RDB_JMS_URL=tcp://<jms IP>:61616 \
    -e JMS2RDB_RDB_URL=jdbc:postgresql://<postgres IP>/log \
    -e JMS2RDB_RDB_USER=log \
    -e JMS2RDB_RDB_PASSWORD=password \
    registry.esss.lu.se/ics-docker/jms2rdb
```


[Docker]: https://www.docker.com
