#!/bin/bash
set -e  

# Ensure the following environment variables are set
# Exit script and container if not set
echo "Checking mandatory environment variables..."
test $JMS2RDB_JMS_URL
test $JMS2RDB_RDB_URL
test $JMS2RDB_RDB_USER
test $JMS2RDB_RDB_PASSWORD

echo "Create the plugin_customization.ini configuration file"
/usr/local/bin/confd -onetime -backend env

echo "Starting JMS2RDB"
exec /opt/jms2rdb/JMS2RDB -pluginCustomization /opt/jms2rdb/plugin_customization.ini
