FROM openjdk:8-jre

LABEL maintainer "benjamin.bertrand@esss.se"

ENV CONFD_VERSION 0.16.0
RUN curl -L -o /usr/local/bin/confd \
    "https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64" \
  && chmod a+x /usr/local/bin/confd

ENV JMS2RDB_VERSION=4.6.1.10
RUN curl -L -O "https://artifactory.esss.lu.se/artifactory/CS-Studio/production/${JMS2RDB_VERSION}/jms2rdb-${JMS2RDB_VERSION}-linux.gtk.x86_64.tar.gz" \
  && tar -C /opt -xzf "jms2rdb-${JMS2RDB_VERSION}-linux.gtk.x86_64.tar.gz" \
  && ln -s "/opt/jms2rdb-${JMS2RDB_VERSION}" /opt/jms2rdb \
  && rm -f "jms2rdb-${JMS2RDB_VERSION}-linux.gtk.x86_64.tar.gz"

RUN useradd -r -M -d /opt/jms2rdb jms2rdb \
  && chown -R jms2rdb:jms2rdb "/opt/jms2rdb-${JMS2RDB_VERSION}"

COPY files/confd /etc/confd
COPY files/start.sh /usr/local/bin/start.sh

USER jms2rdb

CMD ["/usr/local/bin/start.sh"]
